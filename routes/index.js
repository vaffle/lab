var express = require('express');
var router = express.Router();
const morgan = require('morgan');
const cors = require('cors');
let users = require('../user.json');

/* GET home page. */
router.get('/user/add', function(req, res, next) {
    res.render('add.ejs');
});

router.get('/user/edit/:id', function(req, res, next) {
    let userId = req.params.id;
    let data;
    let userIndex = users.findIndex((val) => {
        return val.id == userId;
    });

    if (userIndex == -1)
        res.send("Not Found User");
    else
        res.render('edit.ejs', { data: users[userIndex] });
});

module.exports = router;