var express = require('express');
var router = express.Router();
const morgan = require('morgan');
const cors = require('cors');
let users = require('../user.json');


/* GET users listing. */
router.get('/all/', function(req, res, next) {
    return res.status(200).json({
        code: 1,
        message: 'OK',
        data: users
    })
});

router.get('/show/:id', function(req, res, next) {
    let userId = req.params.id;
    let data;
    let userIndex = users.findIndex((val) => {
        return val.id == userId;
    });


    if (userIndex == -1)
        data = "Not Found User";
    else
        data = users[userIndex];

    return res.status(200).json({
        code: 1,
        message: 'OK',
        data: data
    })
});

router.get('/del/:id', function(req, res, next) {
    const removeId = req.params.id;
    const position = users.findIndex((val) => {
        return val.id == removeId;
    });
    if (position != -1)
        users.splice(position, 1);
    return res.status(200).json({
        code: 1,
        message: 'OK',
        data: users
    })
});

router.post('/add/', function(req, res, next) {
    let user = {}
    user.id = users.length + 1
    user.name = req.body.name;
    user.age = Number(req.body.age);
    user.movie = req.body.movie;
    users.push(user);
    console.log('Users :', user.name, 'Created!')
    return res.status(201).json({
        code: 1,
        message: 'OK',
        data: users
    });
});

router.post('/edit/:id', function(req, res, next) {
    const replaceId = req.params.id;
    const position = users.findIndex(function(val) {
        return val.id == replaceId;
    });
    console.log(users[position]);
    users[position].name = req.body.name;
    users[position].age = Number(req.body.age);
    users[position].movie = req.body.movie;
    return res.status(200).json({
        code: 1,
        message: 'OK',
        data: users[position]
    });
});



module.exports = router;